<?php

class Cow
{
    public $milk;
    public $id;
    public function __construct($id)
    {
        $this->milk = rand(8, 12);
        $this->id = $id; 
    }
}

class Chick
{
    public $eggs;
    public $id;
    public function __construct($id)
    {
        $this->eggs = rand(0, 1);
        $this->id = $id; 
    }
}

class Farm
{
    protected $cowCount;
    protected $chickCount;
    public function __construct($cows, $chicks)
    {
        $this->cowCount = $cows;
        $this->chickCount = $chicks;
    }

    public function getEggs()
    {
        for ($i = 0; $i < $this->chickCount; $i++) {
            $chickNew = new Chick($i);
            $this->storeEggs += $chickNew->eggs;
        }
        return $this->storeEggs;
    }

    public function getMilk()
    {
        for ($i = 0; $i < $this->cowCount; $i++) {
            $cowNew = new Cow($i+100);
            $this->storeMilk += $cowNew->milk;
        }
        return $this->storeMilk;
    }
}

$farm = new farm(10, 20);
echo 'Надоено ' . $farm->getMilk() . ' литров молока и собрано ' . $farm->getEggs()  . ' яиц';